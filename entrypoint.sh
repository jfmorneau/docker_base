#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USER=jf

if [[ ${USER} == "" ]] ; then
    echo 'USER environment variable must be specified'
    exit 1
fi

if [[ -z ${UID} ]] ; then
    UID=1000
fi

if [[ -z ${GID} ]] ; then
    GID=1000
fi

groupadd -g ${GID} ${USER}

useradd -d /home/${USER} -u ${UID} -g $GID -m ${USER}

echo "Starting with UID:GID : ${UID}:${GID}"
export HOME=/home/${USER}

chown ${USER}:${USER} $HOME

cd $HOME

exec /usr/local/bin/gosu ${USER} "$@"
